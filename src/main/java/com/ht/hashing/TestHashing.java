/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.hashing;

/**
 *
 * @author ACER
 */
public class TestHashing {
    public static void main(String[] args) {
        
        Hashing hs = new Hashing(50,"Hello"); 
        hs.put(90, "World");
        hs.put(99, "!!!");

        System.out.println("***********************************************");
        System.out.println(hs.get(50));
        System.out.println(hs.get(90));
        System.out.println(hs.get(99));
        System.out.println();
        
        System.out.println("Remove Value : ");
        System.out.println(hs.get(50));
        System.out.println(hs.get(90));
        System.out.println(hs.remove(99));

      
   
    }
}
