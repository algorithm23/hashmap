/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.hashing;

import static java.util.Objects.hash;

/**
 *
 * @author ACER
 */
public class Hashing {
    
    private int key;
    private String value;
    private String[] arr = new String[200];     

    public Hashing(int key, String value) {
        this.key = key;
        this.value = value;
        arr[hashing(key)] = value;
    }
    
    public void put(int key, String value) {
        arr[hashing(key)] = value;
    }
    
    public String get(int key) {
        return (String) arr[hashing(key)];
    }
    
    public int hashing(int key) {
        return key % arr.length;
    }
    
        public String remove(int i){
        String val =  arr[hash(i)];
        arr[hash(i)] = null;
        return val;
    }
    
}